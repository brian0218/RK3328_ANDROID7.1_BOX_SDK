package com.android.tv.settings.data;

/**
 * @author GaoFei
 * 常量数据
 */
public class ConstData {
	public interface IntentKey{
		String DISPLAY_INFO = "display_info";
		String PLATFORM = "platform";
		String VPN_PROFILE = "vpn_profile";
		String VPN_EXIST = "vpn_exist";
		String VPN_EDITING = "vpn_editing";
		String DISPLAY_ID = "display_id";
		String DISPLAY_NAME = "display_name";
	}
}
